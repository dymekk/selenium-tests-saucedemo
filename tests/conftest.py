import allure
import pytest
from app.browsers.driver_factory import DriverFactory
from app.logger import Logger
import os
from dotenv import load_dotenv

load_dotenv()


def pytest_configure(config):
    config.addinivalue_line(
        "markers", "smoke: mark a test as a smoke test"
    )

class BrowserTest:
    @pytest.fixture()
    def setup(self, request):
        url = os.getenv("url")
        browser = "firefox"  # select browser
        self.driver = DriverFactory.get_driver(browser)

        # Get info about browser's version
        browser_version = self.driver.capabilities['browserVersion']
        allure.dynamic.feature(f"Browser: {browser} (Version: {browser_version})")

        self.driver.implicitly_wait(5)
        self.driver.maximize_window()
        self.driver.get(url)

        def finalizer():
            self.driver.quit()
            Logger.write_log_to_file("Test teardown: Closed the driver\n")

        request.addfinalizer(finalizer)
