import allure
import pytest
from tests.conftest import BrowserTest
from app.pages.login_page_methods import LoginPage
from app.pages.home_page_methods import HomePage
from app.pages.cart_page_methods import CartPage
from app.pages.menu_page_methods import MenuPage
from image_comparison.image_action import ImageActions
from image_comparison.expected_screenshots_path_list import ExpectedScreenshots
from image_comparison.actual_screenshots_dir_list import ActualScreenshotsDirs


@allure.epic("Shopping Test end-to-end")
@pytest.mark.usefixtures("setup")
class TestShopping(BrowserTest):
    """End-to-End Shopping Test add item to order from product's list"""

    def perform_login(self):
        LoginPage(self.driver).login()
        return HomePage(self.driver), CartPage(self.driver), ImageActions(self.driver)

    @pytest.mark.smoke
    @allure.title("Test User login")
    @allure.description("Verify success user login and user can see cart button")
    def test_end_to_end_login(self):
        home_page, _, image_actions = self.perform_login()
        image_actions.take_screenshot("Home")
        assert home_page.action_cart_logo() is True
        assert image_actions.compare_images(ExpectedScreenshots.home_page, ActualScreenshotsDirs.home_page) is True

    @pytest.mark.smoke
    @allure.title("Test User Add Item to Cart and Verify Icon's Value of Cart")
    @allure.description("Verify user can add item to cart and cart icon show added value of items correctly")
    def test_end_to_end_verify_icon_value_of_cart(self):
        home_page, cart_page, _ = self.perform_login()
        home_page.add_item_to_cart()
        initial_cart_items_count = home_page.items_value_on_cart()
        assert initial_cart_items_count == 1

    @pytest.mark.smoke
    @allure.title("Test Cart's Value")
    @allure.description("Verify count of items, cost on cart page are correctly after added items to cart")
    def test_verify_cart_value(self):
        home_page, _, image_actions = self.perform_login()
        home_page.add_item_to_cart()
        item_cost = home_page.item_price()
        initial_cart_items_count = home_page.items_value_on_cart()
        home_page.cart_button()
        cart_page = CartPage(self.driver)
        cart_items_count = cart_page.items_value_on_cart()
        cart_item_price = cart_page.item_price()
        image_actions.take_screenshot("Cart")
        assert cart_items_count == initial_cart_items_count
        assert cart_item_price == item_cost
        assert image_actions.compare_images(ExpectedScreenshots.cart_page, ActualScreenshotsDirs.cart_page) is True

    @pytest.mark.smoke
    @allure.title("Test Add Post Address for sending items")
    @allure.description("Verify user can add personal post info on post page")
    def test_add_post_address_for_sending_items(self):
        home_page, cart_page, image_actions = self.perform_login()
        home_page.add_item_to_cart()
        home_page.cart_button()
        image_actions.take_screenshot("Address")
        cart_page.input_post_info("Test", "Testowicz", "60-000")
        assert cart_page.summarize_page_title() == "Checkout: Overview"
        assert image_actions.compare_images(ExpectedScreenshots.post_address, ActualScreenshotsDirs.post_address_page) is True

    @pytest.mark.smoke
    @allure.title("Test Checkout Order on Summary page")
    @allure.description("Verify order's values on Finish page")
    def test_checkout_order(self):
        home_page, cart_page, image_actions = self.perform_login()
        home_page.add_item_to_cart()
        home_page.cart_button()
        cart_items_count = cart_page.items_value_on_cart()
        cart_item_price = cart_page.item_price()
        cart_page.input_post_info("Test", "Testowicz", "60-000")
        image_actions.take_screenshot("Summary")
        cart_items_count_overview = cart_page.items_value_on_cart()
        subtotal_price = cart_page.subtotal_sum_items_price()
        tax_price = cart_page.value_tax_price()
        total_order_price = cart_page.value_total_price()
        assert subtotal_price == cart_item_price
        assert total_order_price == subtotal_price + tax_price
        assert cart_items_count == cart_items_count_overview
        assert image_actions.compare_images(ExpectedScreenshots.summarize_page, ActualScreenshotsDirs.summary_page) is True

    @pytest.mark.smoke
    @allure.title("Test user can order item")
    @allure.description("Verify Order Finish and text: 'Thank you for your order!'")
    def test_finish_order(self):
        home_page, cart_page, image_actions = self.perform_login()
        home_page.add_item_to_cart()
        home_page.cart_button()
        cart_page.input_post_info("Test", "Testowicz", "60-000")
        cart_page.finish_button()
        image_actions.take_screenshot("Finish")
        assert cart_page.summary_text() == "Thank you for your order!"
        assert image_actions.compare_images(ExpectedScreenshots.finish_page, ActualScreenshotsDirs.finish_page) is True

    @pytest.mark.smoke
    @allure.title("Test user can logout after ordered item")
    @allure.description("Back to Home Page and Logout")
    def test_logout(self):
        home_page, cart_page, image_actions = self.perform_login()
        home_page.add_item_to_cart()
        home_page.cart_button()
        cart_page.input_post_info("Test", "Testowicz", "60-000")
        cart_page.finish_button()
        cart_page.back_to_products_button()

        # Step 8: Open Menu
        home_page.open_menu()

        # Step 9: Logout
        MenuPage(self.driver).logout_user()
        image_actions.take_screenshot("Login")
        assert LoginPage(self.driver).login_page_title() is True
        assert image_actions.compare_images(ExpectedScreenshots.login_page, ActualScreenshotsDirs.login_page) is True
