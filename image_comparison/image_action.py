import os
import allure
from PIL import Image, ImageChops
from datetime import datetime
from image_comparison.actual_screenshots_dir_list import ActualScreenshotsDirs


class ImageActions:

    def __init__(self, driver):
        self.driver = driver

    def compare_images(self, expected_image_path, screenshots_dir, threshold=0):
        latest_screenshot_path = self.get_latest_screenshot(screenshots_dir)
        img1 = Image.open(expected_image_path)
        img2 = Image.open(latest_screenshot_path)
        diff = ImageChops.difference(img1, img2)
        diff = diff.convert('L')
        if diff.getextrema()[1] <= threshold:
            allure.attach.file(latest_screenshot_path, name="Latest Screenshot", attachment_type=allure.attachment_type.PNG)
            return True
        else:
            message = f"Images are not identical: {expected_image_path} vs {latest_screenshot_path}"
            allure.attach.file(latest_screenshot_path, name="Latest Screenshot", attachment_type=allure.attachment_type.PNG)
            raise AssertionError(message)

    def get_latest_screenshot(self, screenshots_dir):
        """ Select last screnshot"""

        files = os.listdir(screenshots_dir)
        files.sort(key=lambda x: os.path.getmtime(os.path.join(screenshots_dir, x)))
        latest_screenshot = files[-1]
        return os.path.join(screenshots_dir, latest_screenshot)

    def take_screenshot(self, name):

        """ Add actual screenshot"""

        timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        if name == "Home":
            screenshots_dir = ActualScreenshotsDirs.home_page
            screen_name = "home_page_actual_screenshot"
            actual_screenshot_path = os.path.join(screenshots_dir, f"{timestamp}-{screen_name}.png")
            self.driver.save_screenshot(actual_screenshot_path)
        if name == "Cart":
            screenshots_dir = ActualScreenshotsDirs.cart_page
            screen_name = "cart_page_actual_screenshot"
            actual_screenshot_path = os.path.join(screenshots_dir, f"{timestamp}-{screen_name}.png")
            self.driver.save_screenshot(actual_screenshot_path)
        if name == "Address":
            screenshots_dir = "actual_screenshots/actual_post_address_cart_page_screenshots"
            screen_name = "post_address_page_actual_screenshot"
            actual_screenshot_path = os.path.join(screenshots_dir, f"{timestamp}-{screen_name}.png")
            self.driver.save_screenshot(actual_screenshot_path)
        if name == "Summary":
            screenshots_dir = ActualScreenshotsDirs.summary_page
            screen_name = "actual_summarize_page_actual_screenshot"
            actual_screenshot_path = os.path.join(screenshots_dir, f"{timestamp}-{screen_name}.png")
            self.driver.save_screenshot(actual_screenshot_path)
        if name == "Finish":
            screenshots_dir = ActualScreenshotsDirs.finish_page
            screen_name = "actual_finish_order_page_screenshot"
            actual_screenshot_path = os.path.join(screenshots_dir, f"{timestamp}-{screen_name}.png")
            self.driver.save_screenshot(actual_screenshot_path)
        if name == "Login":
            screenshots_dir = ActualScreenshotsDirs.login_page
            screen_name = "actual_login_page_screenshot"
            actual_screenshot_path = os.path.join(screenshots_dir, f"{timestamp}-{screen_name}.png")
            self.driver.save_screenshot(actual_screenshot_path)



