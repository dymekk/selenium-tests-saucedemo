class ActualScreenshotsDirs:

    home_page = "actual_screenshots/actual_home_page_screenshots"
    cart_page = "actual_screenshots/actual_cart_page_screenshots"
    post_address_page = "actual_screenshots/actual_post_address_cart_page_screenshots"
    summary_page = "actual_screenshots/actual_summarize_page_screenshots"
    finish_page = "actual_screenshots/actual_finish_order_page_screenshots"
    login_page = "actual_screenshots/actual_login_page_screenshots"