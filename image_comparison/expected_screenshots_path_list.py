class ExpectedScreenshots:

    home_page = "expected_screenshots/home_page_screenshots/home_page_expected_screenshot.png"
    cart_page = "expected_screenshots/cart_page_expected_screenshot/cart_page_expected_screenshot.png"
    post_address = "expected_screenshots/post_address_page_expected_screenshot/post_address_page_expected_screenshot.png"
    summarize_page = "expected_screenshots/summarize_page_expected_screenshots/summarize_page_expected_screenshot.png"
    finish_page = "expected_screenshots/finish_page_expected_screenshot/finish_order_page_actual_screenshot.png"
    login_page = "expected_screenshots/login_page_expected_screenshot/login_page_expected_screenshot.png"