from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager
from selenium.webdriver.chrome.options import Options


class DriverFactory:

    @staticmethod
    def get_driver(browser):
        if browser == "chrome":
            chrome_options = Options()
            chrome_options.add_argument("no-sandbox")
            driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
        elif browser == "firefox":
            driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
        else:
            raise ValueError("Invalid browser specified")

        return driver



