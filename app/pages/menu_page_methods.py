import allure
from app.locators.menu_locators import MenuLocators
from app.logger import Logger


class MenuPage:

    def __init__(self, driver):
        self.driver = driver

    def logout_user(self):
        with allure.step("Verify user logout"):
            self.driver.find_element(*MenuLocators.logout_button).click()
            Logger.write_log_to_file("User logout")
