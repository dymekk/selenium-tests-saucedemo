import allure
from app.logger import Logger
from app.locators.home_page_locators import HomePageLocators


class HomePage:

    def __init__(self, driver):
        self.driver = driver

    def add_item_to_cart(self):
        with allure.step("Add item to cart by click Add button"):
            self.driver.find_element(*HomePageLocators.add_to_cart_button).click()
            Logger.write_log_to_file("Added item to cart")

    def open_item_page(self):
        with allure.step("Open item page by click on Item Name"):
            self.driver.find_element(*HomePageLocators.item_sauce_labs_backpack).click()
            Logger.write_log_to_file("Open item page")

    def items_value_on_cart(self):
        with allure.step("Verify items value on cart"):
            value = self.driver.find_element(*HomePageLocators.cart_icon_value_locator).text
            Logger.write_log_to_file(f"Number of items in the cart's icon: {value}")
            return int(value)

    def cart_button(self):
        with allure.step("Open cart by click on Cart button"):
            self.driver.find_element(*HomePageLocators.cart_button_locator).click()
            Logger.write_log_to_file("Open cart by clicked on cart button")

    def item_price(self):
        with allure.step("Verify item price"):
            cost = self.driver.find_element(*HomePageLocators.item_price_locator).text.replace('$', '')
            Logger.write_log_to_file(f"Item price: ${cost}")
            return float(cost)

    def title_page(self, title):
        with allure.step("Verify Home page title"):
            Logger.write_log_to_file(f"Expected page title: {title}")
            assert self.driver.title == title

    def open_menu(self):
        with allure.step("Open Menu by click on Menu button"):
            self.driver.find_element(*HomePageLocators.menu_button).click()
            Logger.write_log_to_file(f"Open Menu")

    def action_cart_logo(self):
        return self.driver.find_element(*HomePageLocators.cart_button_locator).is_displayed()
