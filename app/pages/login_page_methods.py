import os
import allure
from selenium.webdriver.common import keys
from app.logger import Logger
from app.locators.login_page_locators import LoginPageLocators
from dotenv import load_dotenv

load_dotenv()


class LoginPage:

    def __init__(self, driver):
        self.driver = driver

    @allure.step("Input username '{}'")
    def input_login(self, username):
        self.driver.find_element(*LoginPageLocators.user_name_locator).send_keys(username)

    @allure.step("Input password '{}'")
    def input_password(self, password):
        self.driver.find_element(*LoginPageLocators.password_locator).send_keys(password)

    def action_submit_button(self):
        self.driver.send_keys(keys.Keys.ENTER)
        Logger.write_log_to_file("Pressed 'Enter' to login")

    def click_submit_button(self):
        self.driver.find_element(*LoginPageLocators.submit_button_locator).click()
        Logger.write_log_to_file("Clicked on login button")

    def login_page_title(self):
        return self.driver.find_element(*LoginPageLocators.login_page_title).is_displayed()

    @allure.step("User log in action")
    def login(self):
        username = os.getenv("user_name")
        password = os.getenv("password")
        LoginPage.input_login(self, username)
        LoginPage.input_password(self, password)
        Logger.write_log_to_file(f"My user name {username}, password {password}")
        LoginPage.click_submit_button(self)

