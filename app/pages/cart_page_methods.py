import allure
from app.locators.cart_page_locators import CartPageLocators
from app.locators.home_page_locators import HomePageLocators
from app.logger import Logger


class CartPage:

    def __init__(self, driver):
        self.driver = driver

    def items_value_on_cart(self):
        with allure.step("Verify items value on Cart"):
            cart_items = self.driver.find_elements(*CartPageLocators.cart_item_locator)
            count = len(cart_items)
            Logger.write_log_to_file(
                f"Number of items on the cart: {count}")
            return count

    def item_price(self):
        with allure.step("Verify item price value on Cart"):
            item_price = self.driver.find_element(*HomePageLocators.item_price_locator).text.replace('$', '')
            Logger.write_log_to_file(f"Item price in the cart: ${item_price}")
            return float(item_price)

    def input_post_info(self, first_name, last_name, postal_code):
        with allure.step("Open Post address page by click on Checkout button"):
            self.driver.find_element(*CartPageLocators.checkout_button).click()
            Logger.write_log_to_file("Open post address information by clicked on checkout button")
        with allure.step(
                f"Entered user information first name {first_name}, last name {last_name}, postal code {postal_code} and "
                f"clicked continue"):
            self.driver.find_element(*CartPageLocators.first_name_input).send_keys(first_name)
            self.driver.find_element(*CartPageLocators.last_name_input).send_keys(last_name)
            self.driver.find_element(*CartPageLocators.postal_code_input).send_keys(postal_code)
            Logger.write_log_to_file(
                f"Entered user information first name {first_name}, last name {last_name}, postal code {postal_code} and "
                f"clicked continue")
        with allure.step("Open Summarize page by click on Checkout button"):
            self.driver.find_element(*CartPageLocators.continue_button).click()

    def value_tax_price(self):
        with allure.step("Verify tax price value"):
            tax_price = self.driver.find_element(*CartPageLocators.tax_value).text.replace('Tax: $', '')
            Logger.write_log_to_file(f"Tax price: ${tax_price}")
            return float(tax_price)

    def value_total_price(self):
        with allure.step("Verify Total price"):
            total_price = self.driver.find_element(*CartPageLocators.total_price_value).text.replace(
                'Total: $',
                '')
            Logger.write_log_to_file(f"Total price: ${total_price}")
            return float(total_price)

    def subtotal_sum_items_price(self):
        with allure.step("Verify subtotal price"):
            cart_container = self.driver.find_element(*CartPageLocators.cart_list_items)
            cart_items = cart_container.find_elements(*CartPageLocators.cart_item_locator)
            list_result = []
            for cart_item in cart_items:
                price_element = cart_item.find_element(*HomePageLocators.item_price_locator)
                price_text = price_element.text
                price_value = float(price_text.replace('$', ''))
                list_result.append(price_value)
            total_price = sum(list_result)
            Logger.write_log_to_file(f"Subtotal sum of items price: ${total_price}")
            return total_price

    def finish_button(self):
        with allure.step("Verify order by click on Finish button"):
            self.driver.find_element(*CartPageLocators.finish_button_locator).click()
            Logger.write_log_to_file("Order item by clicked on finish button")

    def back_to_products_button(self):
        with allure.step("Verify user open Products page by click on Back to products button"):
            self.driver.find_element(*CartPageLocators.back_to_products_button).click()
            Logger.write_log_to_file("Open products page by clicked on back to products button")

    def summary_text(self):
        with allure.step("Expected summary text: {}"):
            text = self.driver.find_element(*CartPageLocators.success_order_text).text
            Logger.write_log_to_file(f"Expected summary text: {text}")
            return text

    def summarize_page_title(self):
        with allure.step("Expected Summarize Page title: '{}'"):
            title = self.driver.find_element(*CartPageLocators.summarize_page_title).text
            Logger.write_log_to_file(f"Expected Summarize Page title: {title}")
            return title
