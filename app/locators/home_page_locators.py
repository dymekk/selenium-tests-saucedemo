from selenium.webdriver.common.by import By


class HomePageLocators:

    add_to_cart_button = By.ID, 'add-to-cart-sauce-labs-backpack'
    cart_icon_value_locator = By.CLASS_NAME, "shopping_cart_badge"
    cart_button_locator = By.CLASS_NAME, "shopping_cart_link"
    item_price_locator = By.CLASS_NAME, 'inventory_item_price'
    menu_button = By.ID, "react-burger-menu-btn"
    item_sauce_labs_backpack = By.ID, "item_4_img_link"
    logo_locator = By.CSS_SELECTOR, "div.app_logo"
