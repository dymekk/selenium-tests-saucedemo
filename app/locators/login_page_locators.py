from selenium.webdriver.common.by import By


class LoginPageLocators:
    user_name_locator = By.ID, 'user-name'
    password_locator = By.ID, "password"
    submit_button_locator = By.ID, "login-button"
    login_page_title = By.CSS_SELECTOR, "div.login_container"
