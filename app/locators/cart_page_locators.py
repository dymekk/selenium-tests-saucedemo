from selenium.webdriver.common.by import By


class CartPageLocators:
    cart_item_locator = By.CLASS_NAME, "cart_item"
    checkout_button = By.ID, "checkout"
    first_name_input = By.ID, "first-name"
    last_name_input = By.ID, "last-name"
    postal_code_input = By.ID, "postal-code"
    continue_button = By.ID, "continue"
    tax_value = By.CLASS_NAME, "summary_tax_label"
    total_price_value = By.CSS_SELECTOR, '.summary_info_label.summary_total_label'
    cart_list_items = By.CLASS_NAME, 'cart_list'
    finish_button_locator = By.ID, "finish"
    back_to_products_button = By.ID, "back-to-products"
    success_order_text = By.CLASS_NAME, "complete-header"
    summarize_page_title = By.CSS_SELECTOR, "span.title"
